.TH XCB 1 "Oct 6 1994" "X Version 11"
.SH NAME
xcb \- X Cut Buffers \- Pigeon holes for your cut and paste selections.
.SH SYNOPSIS
.B xcb
[\fIXt option\fP]
[\fB\-l\fP \fIlayout\fP]
[\fB\-n\fP \fIcount\fP]
[\fB\-p\fP|\fB\-s\fP|\fB\-S\fP \fIlist\fP]
[\fB\-r\fP \fIcount\fP]
.SH DESCRIPTION
\fIXcb\fP provides easy access to the cut buffers built into every X server.
It allows the buffers to be manipulated either via the command line,
or with the mouse in a point and click manner.
The buffers can be used as holding pens to store and retrieve
arbitrary data fragments.
Any number of cut buffers may be created,
so any number of different pieces of data can be saved and recalled later.
By default, 8 cut buffers are created.
The program is designed primarily for use with textual data.
.LP
\fIXcb\fP has two modes of operation.
Normally \fIxcb\fP provides an array of windows on your display,
one per cut buffer, tiled horizontally, vertically, or in some
user specified layout.
Each window displays the contents of its respective cut buffer.
Data can be cut from and pasted to the windows in a similar manner
to xterm.
The buffers can also be rotated.
.LP
In task mode, \fIxcb\fP lets you access the cut buffers from the command line.
Cut buffers can be loaded from stdin,
copied or concatenated to stdout,
loaded using the current PRIMARY selection,
or rotated an arbitrary number of positions.
In this mode of operation, \fIxcb\fP performs the requested task
and then exits.
It does not create any windows and
has no interaction with the mouse or keyboard.
.SH OPTIONS
\fIXcb\fP supports the full set of X Toolkit Intrinsics options,
as well as those listed below.
\fIXcb\fP options can appear in any order.
The presence of the
\fB\-p\fP,
\fB\-r\fP,
\fB\-s\fP or
\fB\-S\fP
options causes \fIxcb\fP to execute in task mode,
described above.
.IP "\fB\-l\fP \fIlayout\fP"
This option controls the geometry arrangement of \fIxcb's\fP subwindows.
It is the command line equivalent of the \fB.layout\fP resource,
described below.
.LP
.IP "\fB\-n\fP \fIcount\fP"
Create \fIcount\fP cut buffers.
\fICount\fP can be any integer greater than zero.
This option is the command line equivalent of the \fB.bufferCount\fP
resource, described below.
.LP
.IP "\fB\-u\fP"
Use utf-8 instead of the current locale settings when executing in task mode
and doing I/O.
.LP
.IP "\fB\-V\fP"
Print the xcb release version number and exit immediately.
.LP
.IP "\fB\-p\fP \fIlist\fP"
Print the contents of the listed buffer(s) on stdout.
The buffered data is printed exactly as it is stored in the server.
Selecting two or more buffers has the effect of concatenating
the data on stdout.
The cut buffers are numbered from 0... onwards.
The list can be either a single digit,
a comma separated list of digits,
a range of the form m\-n,
or some combination of lists and ranges.
The buffers are printed in listed order,
so repeated numbers in the list can be used to 
duplicate buffer contents.
.LP
.IP "\fB\-r\fP \fIcount\fP"
Rotate the buffers by \fIcount\fP positions.
\fICount\fP can be any integer, positive or negative.
This option may be used in conjunction with the
\fB\-n\fP \fIcount\fP option to rotate
a specific number of buffers.
If the \fB\-n\fP option is not used, \fIxcb\fP will rotate
the number of buffers given by the \fB.bufferCount\fP resource.
.LP
.IP "\fB\-s\fP \fIlist\fP"
Store the data from stdin in the listed buffer(s).
If the list refers to two or more buffers, the input data is duplicated
in each buffer.
Refer to the \fB\-p\fP option for the definition of a list.
.LP
.IP "\fB\-S\fP \fIlist\fP"
Store the current PRIMARY selection data in the listed buffer(s).
The data is converted to a string representation.
If the list refers to two or more buffers, the PRIMARY selection is
duplicated in each buffer.
Refer to the \fB\-p\fP option for the definition of a list.
Under the \-S option xcb waits for the nominated cut buffer's contents
to change before exiting.  If no change is detected within
3 seconds, xcb exits with a non-zero return code.
.SH WIDGETS and RESOURCES
The \fIxcb\fP widget hierarchy consists of a collection of
custom buffer widgets, one per cut buffer.
In the Athena version of the program, these buffer widgets are all
contained within a single Athena form widget.
In the Motif version of the program, they are 
each enclosed by Motif frame widgets, and the frame
widgets are all contained within a single Motif RowColumn widget.
.LP
The names of the buffer widgets are "buffer0", "buffer1",
"buffer2", .... etc., and their class name is "Buffer".
Each buffer widget supports all the standard core widget
resources, plus the \fB\.foreground\fP and \fB\.fontSet\fP resources.
.LP
Application wide resources are as follows:
.br
.sp 1
.nf
  \fB\.bufferCount\fP (default value 8)
.in +8
This is the number of buffer widgets to create.
Any number of widgets (greater than zero) can be created.
.in -8
.sp 1
  \fB\.layout\fP (default value "h")
.in +8
Only the first character of the resource value is significant.
This is the geometry arrangement to apply in the container widget.
The layout can be "h" (horizontal), "v" (vertical), or some
other value to disable the inbuilt geometry code and specify
the layout via your X resources.  An example is provided in the
application default resources file.
.in -8
.fi
.SH EVENTS and TRANSLATIONS
\fIXcb's\fP input semantics are coded into a Toolkit translation table.
The default bindings have been chosen to conform with the default
configuration of other cut and paste clients, such as xterm.
The bindings may be altered or overridden according to your needs.
The actions functions provided by \fIxcb\fP are:\-
.br
.sp 1
.ns
.ta 16n
.nf
cut()	causes the contents of the chosen cut buffer to become
	the PRIMARY selection.  The window contents, if any,
	are highlighted, and can then be pasted into other
	cut buffers or applications.
.sp 1
paste()	causes the value of the PRIMARY selection to be
	converted into text and pasted into the chosen cut
	buffer, overwriting any previous buffer contents.
	If no PRIMARY selection is present, \fIxcb\fP pastes
	the contents of cut buffer zero into the chosen buffer.
.sp 1
clear()	clears the chosen cut buffer.
.sp 1
rotate(NN)	rotates the cut buffers by NN positions.  NN may
	be any positive or negative number.
.sp 1
refresh()	causes the cut buffer window to be cleared and redrawn.
.sp 1
selreq()	this action function handles paste requests
	from other clients, or other \fIxcb\fP windows.
	It should always be bound to SelectionRequest events.
.sp 1
selclear()	this action function responds to the loss of
	ownership of the PRIMARY selection property.
	It should always be bound to SelectionClear events.
.sp 1
quit()	causes \fIxcb\fP to terminate.
.fi
.ta 8n
.sp 1
The default bindings are as follows:\-
.sp 1
.ta 20n
.nf
<Btn1Down>:		cut() \\n\\
Shift <Btn2Down>:	clear() \\n\\
<Btn2Down>:		paste() \\n\\
Shift <Btn3Down>:	rotate(-1) \\n\\
<Btn3Down>:		rotate(1) \\n\\
<Key>Left:		rotate(-1) \\n\\
<Key>Right:		rotate(1) \\n\\
<Key>Up:		rotate(-1) \\n\\
<Key>Down:		rotate(1) \\n\\
<Key>q:			quit() \\n\\
<SelReq>:		selreq() \\n\\
<SelClr>:		selclear()
.fi
.ta 8n
.sp 1
.SH EXAMPLES
The following are some examples of \fIxcb\fP task mode usage:\-
.sp 1
xcb \-s 0\-7 < /dev/null
.br
This clears the first 8 cut buffers in your server.
.sp 1
echo "G'day." | xcb \-display bigears:0.0 \-s 1,3,5,7
.br
This loads the string "G'day." into four of the cut buffers
on the display "bigears".
.sp 1
xsendevent -win buffer5 '<Btn1Down>'
.br
This uses the program xsendevent to send a synthetic mouse click event
to an xcb subwindow, thereby making that window the owner of
the PRIMARY selection.
.sp 1
ls \`xcb \-p 2,3\`
.br
This produces a listing of all the files named in
cut buffers 2 and 3.
.br
.sp 1
xcb \-p 0\-7 | xcb \-s 0
.br
This concatenates the values in the first 8 cut buffers, and places
the result back in cut buffer zero.
.sp 1
xcb -S 0 && xcb -p 0
.br
The first command copies the current PRIMARY selection
into the first cut buffer.  If the copy succeeds, then
the second command prints that data on stdout.
.br
.sp 1
.nf
for i in 0 1 2 3 4 5 6 7
do
	xcb \-p $i > $HOME/.xcb/$i
done
for i in 0 1 2 3 4 5 6 7
do
	xcb \-s $i < $HOME/.xcb/$i
done
.fi
This first loop saves the contents of each of the cut buffers
in a separate file under your home directory.  The second loop
restores the cut buffer contents from those files.
When placed in your .logout and .login scripts respectively,
the commands are a simple method of preserving your cut buffers
across login sessions.
.sp 1
.nf
function g {
	echo "$1\\\\c" | xcb \-s 7
	grep "$@"
}
function vg {
	vi +/\`xcb \-p 7\` "$@"
}
.fi
These two shell functions exemplify a simple mechanism for saving and
reusing regular expressions.
The first function saves the regex used for grep\-ing into
cut buffer 7.  The second function reuses the most recent
grep regex as a search command in vi.
There is considerable scope for expanding and improving these ideas.
.SH SEE ALSO
xterm(1), xcutsel(1), xclipboard(1), xprop(1)
.br
Athena Widget Set \- C Language Interface
.br
Motif Programmers Reference Guide
.SH AUTHORS
Current Maintainer (I18n version)
.br
Marc Lehmann
.br
E\-mail: pcg\@goof.com
.br
.sp 1
Original Author
.br
Farrell McKay
.br
E\-mail: Farrell.McKay\@mpx.com.au
.br
.sp 1
XView modifications provided by Danny Vanderryn
.br
E\-mail: dvanderr\@us.oracle.com
.SH COPYRIGHT
Copyright (C) 1992,1993,1994 by Farrell McKay.
.br
.sp 1
Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted, provided
that the above copyright notice appears in all copies and that both that
copyright notice and this permission notice appear in supporting
documentation.  This software is provided "as is" without express or
implied warranty.
.SH "BUGS  :\-)"
Xlib's underlying protocol for moving selection data
between client and server can sometimes be slow,
depending on the amount of data involved.
Do not expect fast performance if your selections are
big or you want to store big files in your cut buffers!
("big" means, say, over 10k bytes \- but your mileage
may vary).
