/*
 * xcb: Copyright (C) 1992,1993,1994 by Farrell McKay.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted, provided
 * that the above copyright notice appears in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  This software is provided "as is" without express or
 * implied warranty.
 *
 * This header file contains the definitions and typedefs
 * for the custom cut buffer widget.
 */
#ifndef _cb_h
#define _cb_h

#include <X11/IntrinsicP.h>		/* needed for CorePart et.al.*/

#define	XtNatom				"atom"
#define	XtCAtom				"Atom"


typedef struct _CbClassRec {
	CoreClassPart	core_class;
} CbClassRec, *CbWidgetClass;

typedef struct _CbRec {
	CorePart	core;
	Pixel		fgnd;
	XFontSet	fontset;
	int		font_width;
	int		font_ascent;
	int		font_height;
	GC		gc;
	GC		gc_inv;
	Atom		atom;
} CbRec, *CbWidget;


extern WidgetClass	cbWidgetClass;
extern CbClassRec	cbClassRec;

#endif
